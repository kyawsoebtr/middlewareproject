class UserHeaderMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if request.user.is_authenticated:
            response['X-Username'] = request.user.username
            print(">>>>>>",response['X-Username'])
        else:
            response['X-Username'] = 'Anonymous'
        return response
    