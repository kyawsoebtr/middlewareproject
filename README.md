# middlewareproject
### A brief Intro
    This app is for customizing the logs in the grafana loki based on the nginx logs.

    Backend - A simple applications adding a middleware to know which user is logging in the system.
    Nginx - Serving the backend as proxy and additionally capture user from the backend.
    Promtail - It is responsible for gathering and forwarding log data from various sources like log file and kubernetes pod etc.
    Loki - Handling logs data and use for the solution for storing, querying, and exploring logs. It uses the PromQL Langage
    Grafana - It provides a user-friendly interface for querying and visualizing log data stored in Loki.


## Getting started

```docker compose build ``` 
``` docker compose up``` 

```docker exec -it backend sh```
```python manage.py makemigrations```
```python manage.py migrate```
```python manage.py createsuperuser```

Go to ```localhost:3000``` for Grafana Dashboard
Click to menu Icon
![Alt text](./pictures/explore.png)

After that choose the data source "Loki" and type the query... and Click Run

![Alt text](./pictures/query.png)

Go to menu and Click Dashboard
![Alt text](./pictures/dashboard.png)

Click New Dashboard
![Alt text](./pictures/new-dashboard.png)

Add Visualization
![Alt text](./pictures/visualization.png)

Choose the Loki Source
![Alt text](./pictures/choose-loki.png)

Write the query as shown in the panel
![Alt text](./pictures/write_the_query.png)

Click the Label Browser on above picture... You can see the visuzalize in UI or you can write your own query and CLick 'Show Logs Button'
![Alt text](./pictures/label_browser.png)

Can choose Desire Log Format.. In here choose log
![Alt text](./pictures/choose_log_format.png)

And It will appear like this
![Alt text](./pictures/first_log_result.png)

After that in the right side add you panel title and Click Save... Can Have a dashboard like this

![Alt text](./pictures/result-dashboard-panel.png)

To add a new panel in  dashboard.. Click Add and visualization as below
![Alt text](./pictures/new-panel.png)

Let write some query in the new panel... Please choose code and write desired query. Suppose getting all the 404 logs .... 
``` {filename="/var/log/nginx/access.log",job="backend"}| pattern `<_> - - <_> "<method> <_> <_>" <status> <_> <_> "<_>" <_> "<_>"`| status="404"```

![Alt text](./pictures/404_logs.png)


Get All the POST request
![Alt text](./pictures/get_all_the_post_request.png)

All the Authen Logs 
```{filename="/var/log/nginx/access.log",job="backend"}| pattern `<remote_addr> - - <time_local> "<method> <request> <protocol>" <status> <body_bytes_sent> <http_referer> "<http_user_agent>" "<sent_http_x_username>"` | request="/admin/login/?next=/admin/" | status="302" ```

When Checking out authen logs,, checked with above code and can see all the authen logs... Here is two user logging -> johndoe and shamu
![Alt text](./pictures/authen_logs.png)


Unauthorized Log checking 
``` {filename="/var/log/nginx/access.log",job="backend"}| pattern `<remote_addr> - - <time_local> "<method> <request> <protocol>" <status> <body_bytes_sent> <http_referer> "<http_user_agent>" "<sent_http_x_username>"`  | status="403"```

![Alt text](./pictures/unauthorized_request.png)


Custom Dashboard Design
![Alt text](./pictures/custom_dashboard_design.png)



